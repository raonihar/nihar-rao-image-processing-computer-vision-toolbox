/*
Author:-Nihar Rao
College:-Manipal Institute of Technology,Manipal
Email:- raonihar1997@gmail.com or niharsrao@gmail.com
Phone:- +918652328719/+919833116086
sources used: opencv documentation and answers.opencv.in

*/


#include <opencv2/core/core.hpp>
#include "opencv2/core.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/objdetect.hpp"
#include "opencv2/face/facerec.hpp"
#include "opencv2/face.hpp"
#include<bits/stdc++.h>

using namespace cv;
using namespace std;
using namespace cv::face;

/*declaration of global variables */


int image_flag=1;//flag for depicting success/failure of reading a image
CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;
String face_cascade_name, eyes_cascade_name;
vector<int> labels;
Ptr<face::FaceRecognizer> model = face::createFisherFaceRecognizer();
vector<vector<Point> >contours;



/*end of declaraton of global variables*/


//function for training with jellysfish images
int train_jelly(vector<Mat>& images)
{

   Mat image=imread("jelly1.jpg", CV_LOAD_IMAGE_GRAYSCALE);
   if(image.data)
   {images.push_back(image); labels.push_back(0);}
   else
   return 0;
   image=imread("jelly2.jpg", CV_LOAD_IMAGE_GRAYSCALE);
   if(image.data)
   {images.push_back(image); labels.push_back(1);}
   else
   return 0;
   image=imread("jelly3.jpg", CV_LOAD_IMAGE_GRAYSCALE);
   if(image.data)
   {images.push_back(image); labels.push_back(2);}
   else
   return 0;
   image=imread("jelly4.jpg", CV_LOAD_IMAGE_GRAYSCALE);
   if(image.data)
   {images.push_back(image); labels.push_back(2);}
   else
   return 0;
   for(int i=0;i<images.size();i++)
       cv::resize(images[i],images[i], images[0].size(), 1.0, 1.0, INTER_CUBIC);

    model->train(images, labels);
    return 1;



}

void find_moments( Mat gray)
{
    Mat canny_output;

    vector<Vec4i> hierarchy;

    /// Detect edges using canny
    Canny( gray, canny_output, 50, 150, 3 );
    /// Find contours
    findContours( canny_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

    /// Get the moments
    vector<Moments> mu(contours.size() );
    for( int i = 0; i < contours.size(); i++ )
    { mu[i] = moments( contours[i], false );
      Moments m=mu[i];
      cv:: Point center(m.m10/m.m00, m.m01/m.m00);
      //draw red cross
      string label="x";
      putText(gray, label, Point(center.x, center.y), FONT_HERSHEY_PLAIN, 1.0, CV_RGB(255,0,0), 2.0);


    }







}


int get_Jellies(Mat frame)
{
 try{
    vector<Mat> images;
       if(train_jelly(images))
       {

              face_cascade_name = "../../OpenCV/data/haarcascades/haarcascade_frontalface_alt.xml";//finding the haarcascade_xml file,please put exact location of this file to make the program work!
              if (!face_cascade.load(face_cascade_name)){
        cout<<"Error loading ,please make sure that the data path is correct!"<<endl;
        return -2;
             }




           Mat original = frame.clone();//clone the imafe
         Mat gray;
         cvtColor( frame, gray, COLOR_BGR2GRAY );//convert to grayscale


          vector< Rect_<int> > faces;
         face_cascade.detectMultiScale(gray, faces);

             int im_width = images[0].cols;//get height and widthe for resizing as Fisher alorithm works for same size
             int im_height = images[0].rows;


        for(int i = 0; i < faces.size(); i++) {
            // Process face by face:
            Rect face_i = faces[i];

            Mat face = gray(face_i);
             std::vector<Rect> eyes;


             Mat face_resized;
            //resze as fischer algorithm works for same size images
            cv::resize(face, face_resized, Size(im_width, im_height), 1.0, 1.0, INTER_CUBIC);

            int prediction = model->predict(face_resized);

            rectangle(original, face_i, CV_RGB(0, 255,0), 1);
            //detect centroid

            //set contour and mark centroid for eac,see function
           find_moments(face);




        }

         imshow( "faces", frame );
          waitKey(0);





















    }
    }catch(cv::Exception& e)
    {
       cout<<"Exception occured "<<e.msg<<endl;



    }
   }








int main()
{
    Mat Image;
    Image = imread("jellyfish.jpg", CV_LOAD_IMAGE_UNCHANGED);
  if(!Image.data )                              // Check for invalid input
    {
        image_flag=0;//set flag to 0

    }




    if(image_flag==1)
    {
      cout<<"welcome"<<endl;
     get_Jellies(Image);



    }else
    cout<<"************\n could not load image ,make sure all libraries are linked properly!"<<endl;



    return 0;
}

