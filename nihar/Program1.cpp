/*
Author:-Nihar Rao
College:-Manipal Institute of Technology,Manipal
Email:- raonihar1997@gmail.com or niharsrao@gmail.com
Phone:- +918652328719/+919833116086
sources used: opencv documentation and answers.opencv.in

*/



#include <opencv2/core/core.hpp>
#include "opencv2/core.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/objdetect.hpp"
#include "opencv2/face/facerec.hpp"
#include "opencv2/face.hpp"
#include<bits/stdc++.h>

using namespace cv;
using namespace std;
using namespace cv::face;


/*declaration of global variables */


int image_flag=1;//flag for depicting success/failure of reading a image
CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;
String face_cascade_name, eyes_cascade_name;
vector<int> labels;
Ptr<face::FaceRecognizer> model = face::createFisherFaceRecognizer();
vector<vector<Point> >contours;



/*end of declaraton of global variables*/


void find_moments( Mat gray,Mat frame )
{
    Mat canny_output;

    vector<Vec4i> hierarchy;

    /// Detect edges using canny
    Canny( gray, canny_output, 50, 150, 3 );
    /// Find contours
    findContours( canny_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

    /// Get the moments
    vector<Moments> mu(contours.size() );
    for( int i = 0; i < contours.size(); i++ )
    { mu[i] = moments( contours[i], false );
      Moments m=mu[i];
      cv:: Point center(m.m10/m.m00, m.m01/m.m00);
      int newval[3];

        frame.at<cv::Vec3b>(center.y,center.x)[0] = newval[0];
        frame.at<cv::Vec3b>(center.y,center.x)[1] = newval[1];
        frame.at<cv::Vec3b>(center.y,center.x)[2] = newval[2];

        //print

        cout<<"the rgb value at centroid of the eyes is"<<newval[0]<<" "<<newval[1]<<" "<<newval[2]<<endl;

    }







}

//function for training with ellen eye closeup images
int train_ellen(vector<Mat>& images)
{

   Mat image=imread("ellen1.jpg", CV_LOAD_IMAGE_GRAYSCALE);
   if(image.data)
   {images.push_back(image); labels.push_back(0);}
   else
   return 0;
   image=imread("ellen2.jpg", CV_LOAD_IMAGE_GRAYSCALE);
   if(image.data)
   {images.push_back(image); labels.push_back(1);}
   else
   return 0;
   image=imread("ellen3.jpg", CV_LOAD_IMAGE_GRAYSCALE);
   if(image.data)
   {images.push_back(image); labels.push_back(2);}
   else
   return 0;

   for(int i=0;i<images.size();i++)
      cv::resize(images[i],images[i],images[0].size(), 1.0, 1.0, INTER_CUBIC);

    model->train(images, labels);

    return 1;



}

int get_Elleneyes(Mat frame)
{
 try{

    vector<Mat> images;
       if(train_ellen(images))
       {
          cout<<1;
         eyes_cascade_name = "../../OpenCV/data/haarcascades/haarcascade_eye_tree_eyeglasses.xml";
         face_cascade_name = "../../OpenCV/data/haarcascades/haarcascade_frontalface_alt.xml";//finding the haarcascade_xml file,please put exact location of this file to make the program work!
         if (!face_cascade.load(face_cascade_name)){
        cout<<"Error loading ,please make sure that the data path is correct!"<<endl;
        return -2;
       }



         if( !eyes_cascade.load( eyes_cascade_name ) ){ cout<<("--(!)Error loading eyes cascade\n"); return -1; };

           Mat original = frame.clone();//clone the imafe
         Mat gray;
         cvtColor( frame, gray, COLOR_BGR2GRAY );//convert to grayscale


          vector< Rect_<int> > faces;
         face_cascade.detectMultiScale(gray, faces);

             int im_width = images[0].cols;//get height and widthe for resizing as Fisher alorithm works for same size
             int im_height = images[0].rows;


        for(int i = 0; i < faces.size(); i++) {
            // Process face by face:
            Rect face_i = faces[i];

            Mat face = gray(face_i);
             std::vector<Rect> eyes;

            eyes_cascade.detectMultiScale( face, eyes, 1.1, 2, 0 |CASCADE_SCALE_IMAGE, Size(30, 30) );
             Mat face_resized;
            //resze as fischer algorithm works for same size images
            cv::resize(face, face_resized, Size(im_width, im_height), 1.0, 1.0, INTER_CUBIC);

            int prediction = model->predict(face_resized);

            rectangle(original, face_i, CV_RGB(0, 255,0), 1);
            //detect eyes

            for ( size_t j = 0; j < eyes.size(); j++ )
           {
            Point eye_center( faces[i].x + eyes[j].x + eyes[j].width/2, faces[i].y + eyes[j].y + eyes[j].height/2 );
            int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
            circle( frame, eye_center, radius, Scalar( 255, 0, 0 ), 4, 8, 0 );
           }
        }


        //set contour and find centroid and print it
        find_moments(gray,frame);

        imshow( "faces", frame );
            waitKey(0);















       }


    }catch(cv::Exception& e)
    {
       cout<<"Exception occured "<<e.msg<<endl;



    }


}

//function for detecting faces in the input image

void detect_faces(Mat frame)
{
   face_cascade_name = "../../OpenCV/data/haarcascades/haarcascade_frontalface_alt.xml";//finding the haarcascade_xml file,please put exact location of this file to make the program work!
   if (!face_cascade.load(face_cascade_name)){
        cout<<"Error loading ,please make sure that the data path is correct!"<<endl;
        return;
    }


    std::vector<Rect> faces;
    Mat frame_gray;
    cvtColor( frame, frame_gray, COLOR_BGR2GRAY );//convert to grayscale
    equalizeHist( frame_gray, frame_gray );
    //-- Detect faces
    face_cascade.detectMultiScale( frame_gray, faces, 1.1, 2, 0|CASCADE_SCALE_IMAGE, Size(30, 30) );
    //detect all faces
    for ( size_t i = 0; i < faces.size(); i++ )
    {
        Point center( faces[i].x + faces[i].width/2, faces[i].y + faces[i].height/2 );
        ellipse( frame, center, Size( faces[i].width/2, faces[i].height/2 ), 0, 0, 360, Scalar( 255, 0, 255 ), 4, 8, 0 );
        Mat faceROI = frame_gray( faces[i] );

    }
    imshow( "faces", frame );
    waitKey(0);

}

int main()
{
    Mat Image;
    Image = imread("oscarSelfie.jpg", CV_LOAD_IMAGE_UNCHANGED);
  if(!Image.data )                              // Check for invalid input
    {
        image_flag=0;//set flag to 0

    }




    if(image_flag==1)
    {
      cout<<"welcome"<<endl;
      int option;
      do{
      cout<<"enter 1 for All faces detection\n enter 2 for ellen Degeneres eye detection and for getting RGB value at centroid of Ellen's eyes\n 0 for exit"<<endl;
      cin>>option;
          switch(option)
          {
           case 1:
             detect_faces(Image);
             break;
           case 2:

              get_Elleneyes(Image);
              break;

            default:
               cout<<"please enter numbers between 0-4"<<endl;
               break;



          }


       }while(option!=0);



    }else
    cout<<"************\n could not load image ,make sure all libraries are linked properly!"<<endl;



    return 0;
}


